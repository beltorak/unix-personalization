# Initial VM Setup

## Create Ubuntu 16 Server VM

Create a new VirtualBox VM using the create-vm script.

Run the VM with the Ubuntu Server 16.04 ISO and set up the linux install with the following details:

* EFI
* 1GB RAM
* One 6GB harddrive
 * Two network cards
  - 1st as NAT; use as default during install
  - 2nd as Host Only in 192.168.56.0/24
 * Partitions:
  - 1) 400MB as ext4 mounted at /boot
  - 2) 550MB as EFI System Partition mounted at /boot/efi
  - 3) 95% as Physical Volume for LVM
  - 4) (remainder) as Swap
  * LVM Setup
   - Create Volume Group vg0
   - Add Physical Volume (created above)
   - Add Logical Volume lv0
* Software
 - OpenSSH Server
 - Host for Virtual Machines

### Note: The VirtualBox EFI implementation does not follow all standards. The first boot will fail.
 At the EFI prompt, do `FS0:` and `.\EFI\ubuntu\grub[TAB]`.

## Provision VM

* Transfer script `vm-prep` to VM and run as root.

This script:

- fixes the boot
- fixes the networking (enables the host-only network card at 192.168.56.101)
- sets up some basic niceties such as case-insensitive tab complete and a colorful prompt.
- Creates helpful scripts in root's home `/root` for 
 - performing a full update including kernels
 - script that removes SSH keys - use this before exporting an appliance

At this point you may shut off the machine and export it to save a general purpose linux VM.

## Setup for docker

* Transfer docker-prep script VM
* Run docker-prep as root. This script makes a few important changes to the base system:
 - Changes the host-only IP to 192.168.56.100
 - Changes the hostname to "dvm"
 - Installs the docker tools
 - installs the internal USPS CA certs
