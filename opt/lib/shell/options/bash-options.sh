#! /bin/bash
#
# A simple option parser for bash.
#
# Features:
# - Options, flags, and arguments set script-specified variables
# - Long (--opt) and short (-o) opts
# - Flags (options without user supplied value)
# - Positional arguments are named
# - Usage is a simple { opt_parse "$@" }
#
# Designed Limitations:
# (These can be rightly classified as bugs but I don't know if I will ever fix them)
# - No validation of missing option argument;
#   if an option takes an arugment but is followed by another option,
#   the second option is assumed to be the argument of the first option.
# - No support for collapsing single character opts;
#   e.g.: "-a -x" cannot be specified as "-ax"
# - No support for multi-values;
#   e.g.: "-s 1 -s 2", the '2' overrides the '1'
#   A possible workaround is to specify a non-space delimiter, and
#   set your real variable to the array substitution. For example:
#   you want "-s" (sources) to take multiple names. The docs
#   would say "separate multiple sources with ','". The option parser
#   would set "sources=one,two,three". Your script would
#   do something like "sources=( ${sources//,/ } ).
#   The limitation of this approach is that it is not easy to
#   have multi values with embedded spaces.
# - No validation that options (long or short) are unique.
#
# TODO:
# - Positional varargs
#   That is, positional arguments that don't have a name end up in
#   a script-specified array, such as script_arg_vars[*]
# - Extract help-request detection from the arguments parser.
#   Currently it stops parsing on --help,-h,-H, or -?; prints help and exits.
# - Support standard "end of options" flag (--) to indicate that only
#   positional arguments follow. Useful when the positional argument starts with '-'.
# - Support "long equals" style, such as "--option-name=value";
#   long options are like short options, the next command line token is the option value.
# - Support for enumerated option values; i.e.: only allow certain values for an option.
#   This can be worked around by providing multiple flags that affect the same var,
#   or by abusing the lack of "long equals" to make an flag appear to be an enumerated option.
# - Allow providing a callback function to execute instead of just setting a var.

######
# This is meant to be overridden by the script.
# It is executed when help is requested (--help,-h,-H, or -?) or when the
# option parser detects a user input error.
cat_help () {
cat<<_EOF
Usage: $(basename $0) [options] [flags] [arguments]

Options:
$(_cat_opts | sed -e 's/^/    /')

Flags:
$(_cat_flags | sed -e 's/^/    /')

Arguments:
$(_cat_args | sed -e 's/^/    /')

_EOF
}

######
# This is meant to be overridden by the script.
# This is prepended to every variable set by the options parser
# For example, if set to '_script_arg_', then to get the "username"
# option, access $_script_arg_username
OPT_VAR_PREFIX=

# These can be overridden by the script.
# It defines the exit codes for when the user does
# not supply the options correctly.
OPT_EXIT_BAD_OPT=1 # Unknown option or missing value
OPT_EXIT_BAD_ARG=1 # Too many positional parameters
OPT_EXIT_MISSING_ARG=1 # missing a required positional parameter

# These should be provided by the script
OPT_FLAGS=()
OPT_NAMES=()
OPT_ARGS=()

############################################
### Example Declarations; these should be
### declared right under the help synopsis
### for the edification of whoever opens your script.
###
### You do not have to place your definitions in a function,
### this example merely does so to prevent polluting the user
### script with the example.
############################################
function _opt_example_declarations () {
# FLAGS are switches; they do not take arguments. Either the are present or they are absent.
# Note that the proto version does not have a long option trigger,
# and the ssl flag does not have a short option trigger.
#   1: the short flag char; triggered with '-'
#   |	2: the long flag word; triggered with '--'
#   |	|			3: The variable name to set when the flag is present
#   |	|			|					 4: The value to set the variable to
#   |	|			|					 |			5: The help description
#   |	|			|					 |			|
OPT_FLAGS=(
	s	silent		trace_level			 0			"Less Verbose"
	q	quite		trace_level			-1			"Most least noise"
	v	verbose		trace_level			 1			"noizy"

	1	""			proto_version		 1			"Use protocol 1 only"
	2	""			proto_version		 2			"Use protocol 2 only"

	""	ssl			use_ssl				 why-not	"Use SSL encryption"
)

# Named arguments, these require a parameter
#	1: The short option char; triggered with '-'
#	|	2: The long option word; triggered with '--'
#	|	|				3: The variable to set; the value is the next arg on the command line
#	|	|				|						4: The help description
#	|	|				|						|
OPT_NAMES=(
	u	user			auth_username			"User name for auth"
	P	passfile		auth_password_file		"File containing password"
)

# Positional arguments
# These are the ordered parameters on the command line
# The order is determined by the order of the argument descriptor in this array.
#   1: The name of the argument
#   |            2: The help  description
#   |            |
OPT_ARGS=(
	source		"Location to grab"
	dest		"Place to dump"
)
}

############
##### Herein lies the script; not meant to be modified.

## Internal Bookeeping
_OPT_FLAG_POS_SHORT=0
_OPT_FLAG_POS_LONG=1
_OPT_FLAG_POS_VAR=2
_OPT_FLAG_POS_VAL=3
_OPT_FLAG_POS_DESC=4
_OPT_FLAG_PARTS=5

_OPT_ARG_POS_NAME=0
_OPT_ARG_POS_DESC=1
_OPT_ARG_PARTS=2

_OPT_NAME_POS_SHORT=0
_OPT_NAME_POS_LONG=1
_OPT_NAME_POS_VAR=2
_OPT_NAME_POS_DESC=3
_OPT_NAME_PARTS=4

_cat_opts () {
	local n
	local i
	for (( n = 0; n < (${#OPT_NAMES[@]} / _OPT_NAME_PARTS); n++ )); do
		i=$((n * _OPT_NAME_PARTS))
		printf "%-7s %-15s %s\n" \
				"${OPT_NAMES[i+_OPT_NAME_POS_SHORT]:+-${OPT_NAMES[i+_OPT_NAME_POS_SHORT]}}" \
				"${OPT_NAMES[i+_OPT_NAME_POS_LONG]:+--${OPT_NAMES[i+_OPT_NAME_POS_LONG]}}" \
				"${OPT_NAMES[i+_OPT_NAME_POS_DESC]}"
	done
}

_cat_flags () {
	local n
	local i
	for (( n = 0; n < (${#OPT_FLAGS[@]} / _OPT_FLAG_PARTS); n++ )); do
		i=$((n * _OPT_FLAG_PARTS))
		printf "%-7s %-15s %s\n" \
				"${OPT_FLAGS[i+_OPT_FLAG_POS_SHORT]:+-${OPT_FLAGS[i+_OPT_FLAG_POS_SHORT]}}" \
				"${OPT_FLAGS[i+_OPT_FLAG_POS_LONG]:+--${OPT_FLAGS[i+_OPT_FLAG_POS_LONG]}}" \
				"${OPT_FLAGS[i+_OPT_FLAG_POS_DESC]}"
	done
}

_cat_args () {
	local n
	local i
	for (( n = 0; n < (${#OPT_ARGS[@]} / _OPT_ARG_PARTS); n++ )); do
		i=$((n * _OPT_ARG_PARTS))
		printf "%-22s %s\n" \
				"${OPT_ARGS[i+_OPT_ARG_POS_NAME]}" \
				"${OPT_ARGS[i+_OPT_ARG_POS_DESC]}"
	done
}

# 1: The option variant 'long' 'short'
# 2: The option or flag name
# Returns __opt_type: 'option' 'flag'
#         __opt_num: The option number in the order declared in the arrays
#    false if the arg is not an option or flag
_opt_type () {
	local variant=$1
	local name=$2

	local i
	local pos

	pos=$_OPT_FLAG_POS_SHORT
	[[ $variant = long ]] && pos=$_OPT_FLAG_POS_LONG

	for (( __opt_num = 0; __opt_num < (${#OPT_FLAGS[@]} / $_OPT_FLAG_PARTS); __opt_num++ )); do
		i=$((__opt_num * _OPT_FLAG_PARTS))
		[[ ${OPT_FLAGS[i+pos]} = $name ]] && {
			__opt_type='flag'
			true
			return
		}
	done

	pos=$_OPT_NAME_POS_SHORT
	[[ $variant = long ]] && pos=$_OPT_NAME_POS_LONG
	for (( __opt_num = 0; __opt_num < (${#OPT_NAMES[@]} / $_OPT_NAME_PARTS); __opt_num++ )); do
		i=$((__opt_num * _OPT_NAME_PARTS))
		[[ ${OPT_NAMES[i+pos]} = $name ]] && {
			__opt_type='option'
			true
			return
		}
	done

	__opt_type= #undef
	false
}

# 1: The option number as declared in the arrays
# 2: The option value
#    If the value is empty then this returns false
_opt_set () {
	local num=$1
	local val="$2"

	[[ -z "$val" ]] && {
		false
		return
	}

	local i=$((num * _OPT_NAME_PARTS))
	printf -v ${OPT_VAR_PREFIX}${OPT_NAMES[i+_OPT_NAME_POS_VAR]} "%s" "$val"
	true
	return
}

# 1: The option number as declared in the arrays
# 2: The option name
# This is a no-op if the name is not valid
_opt_set_flag () {
	local num=$1
	local name=$2

	local i=$((num * _OPT_FLAG_PARTS))
	printf -v ${OPT_VAR_PREFIX}${OPT_FLAGS[i+_OPT_FLAG_POS_VAR]} "%s" ${OPT_FLAGS[i+_OPT_FLAG_POS_VAL]}

	true
}

# Sets a positional argument
# 1: the argument value
# false if there were too many arguments already set
_opt_set_arg () {
	[[ -z "$_OPT_ARG_INDEX" ]] && _OPT_ARG_INDEX=0
	local val="$1"

	if [ $_OPT_ARG_INDEX -ge $(( ${#OPT_ARGS[@]} / _OPT_ARG_PARTS )) ]; then
		false
		return
	fi

	printf -v ${OPT_VAR_PREFIX}${OPT_ARGS[$((_OPT_ARG_PARTS * (_OPT_ARG_INDEX++) ))]} "%s" "$val"
	true
}

opt_parse () {
	while [ ! -z "${1:-}" ]; do
		case "$1" in
			--help|-h|-H|"-?")
				cat_help
				exit 0
				;;
			-*)
				local arg=${1:1}
				local variant=short

				# is it a long opt? (uses '--')
				[[ ${arg:0:1} = - ]] && {
					variant=long
					arg=${arg:1}
				}

				# get the option type or bail
				_opt_type $variant $arg || {
					echo "Unrecognized Option: $1" >&2
					cat_help >&2
					exit $OPT_EXIT_BAD_OPT
				}

				# set flag or set option
				if [ $__opt_type = 'flag' ]; then
					_opt_set_flag $__opt_num $arg
				else
					_opt_set $__opt_num "$2" || {
						echo "Option $1 requires a value" >&2
						cat_help >&2
						exit $OPT_EXIT_BAD_OPT
					}
					shift
				fi
				;;
			*)
				_opt_set_arg "$1" || {
					echo "Too Many Positional Arguments" >&2
					cat_help >&2
					exit $OPT_EXIT_BAD_ARG
				}
				;;
		esac
		shift
	done
}

#_opt_example_declarations
#opt_parse "$@"
#declare | grep -E ^$OPT_VAR_PREFIX
