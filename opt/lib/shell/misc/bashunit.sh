#!/bin/bash

set -o errexit

# test files have the pattern ./tests/**/XXXX-tests.sh
# test cases have the pattern test_xxxxxx
# the file _tested_scripts.sh is sourced to source functions under test
# __BASHUNIT__ is set to allow skipping the main method while under test

SCRIPT_SELF="$(readlink -f "$BASH_SOURCE")"
SCRIPT_NAME="$(basename "$SCRIPT_SELF")"
SCRIPT_HOME="$(dirname "$SCRIPT_SELF")"

test_sources="_tested_scripts.sh"

/bin/rm -rf target/testresults
mkdir -p target/testresults/output
T=$(mktemp -t -d tmp."$SCRIPT_NAME".XXXXXX)
trap "/bin/rm -rf \"$T\"" 0

assert_equals () {
	local msg="$1"
	local expected="$2"
	local actual="$3"
	
	if [ ! -z "$msg" ]; then
		msg="$msg: "
	fi
	if [ "$expected" != "$actual" ]; then
		printf "%sexpected <%s>; got <%s>\\n" "$msg" "$expected" "$actual" >&3
		testvar_load assertions_failed
		testvar_save assertions_failed $((assertions_failed + 1))
	fi
}

assert_var_equals () {
	local msg="$1"
	local -n expected="$2"
	local -n actual="$3"

	local expected_str="$(declare -p "${!expected}")"
	local actual_str="$(declare -p "${!actual}")"
	if [ "${expected_str#*=}" != "${actual_str#*=}" ]; then
		printf '%sexpected <%s>; got <%s>\n' "${msg:+$msg: }" "$expected_str" "$actual_str" >&3
		testvar_load assertions_failed
		testvar_save assertions_failed $((assertions_failed + 1))
	fi
}

# testvar_save vname
# testvar_save vname 0
testvar_save () {
	[[ ! -z "$2" ]] && printf -v $1 "%s" "$2"
	echo "${!1}" > $T/testvar.$1
}

testvar_load () {
	[[ -f $T/testvar.$1 ]] || return 0
	printf -v $1 $(/bin/cat $T/testvar.$1)
}

list_tests () {
	if [ -z "$1" ]; then
		find tests/ -type f -name "*-tests.sh" > $T/test-scripts
	else
		while [ ! -z "$1" ]; do
			echo "tests/$1"
			shift
		done > "$T/test-scripts"
	fi
}

runtests () {
	testvar_save testcases 0
	testvar_save failures 0
	testvar_save warnings 0
	testvar_save errors 0
	
	while read testscript; do
		testvar_load failures
		testvar_load testcases
		testvar_load warnings
		testvar_load errors
		
		outfile="$(echo "$testscript" | sed -e 's/\//./g ; s/[-_]tests.sh$// ; s/^tests.//')"
		script_testcases=0
		(
			source "$testscript" || {
				echo "! WARNING: failed to source $testscript"
				testvar_save warnings $((warnings + 1))
				exit 1
			}
			TEST_SCRIPT_DIR="$(/usr/bin/dirname "$testscript")"
			TEST_SCRIPT_NAME="$(/usr/bin/basename "${testscript%.*}")"
			for testcase in $(declare -F | cut -f 3 -d ' ' | grep ^test_); do
				script_testcases=$((script_testcases + 1))
				(
					TEST_NAME="${TEST_SCRIPT_NAME}.${testcase}"
					outfile=${outfile}.${testcase/test_}
					testvar_save assertions_failed 0
					stdout=target/testresults/output/${outfile}.out
					stderr=target/testresults/output/${outfile}.err
					exec 3>&1
					exec 4>&1
					(
						command_not_found_handle () {
							echo "(${BASH_SOURCE[1]}:${BASH_LINENO[0]}): Command Not Found: $1" >&4
							exit 1
						}
						source "$testscript"
						set -o errexit
						set -o pipefail
						set -o xtrace
						export __BASHUNIT__=1
						[[ -f "$test_sources" ]] && source "$test_sources"
						declare -F before_each >/dev/null && { before_each || exit ; } || /bin/true
						$testcase
						declare -F after_each >/dev/null && { after_each || exit 0 ; } || /bin/true
						exit
					) 1>$stdout 2>$stderr 3>$T/assert 4>$T/test-errors
					exit_code=$?
					exec 3>&-
					exec 4>&-
					test_error=$(wc -c < $T/test-errors)
					testvar_load assertions_failed
					if ((exit_code)) || ((test_error)); then
						printf "!   ERROR: %s " "$outfile"
						if ((test_error)); then
							/bin/cat $T/test-errors
						fi
						echo
						if ((exit_code)); then
							echo "=========================================="
							/bin/cat $stderr
							echo  "=========================================="
						fi
						testvar_load errors
						testvar_save errors $((errors + 1))
					elif ((assertions_failed)); then
						printf "! FAILURE: %s \\n>>> \\n%s\\n<<< \\n" "$outfile" "$(sed -e 's/^/    /' < $T/assert)"
						testvar_load failures
						testvar_save failures $((failures + 1))
					else
						printf "  SUCCESS: %s\\n" "$outfile"
					fi
				)
			done
			if ((script_testcases)); then
				testcases=$((testcases + script_testcases))
			else
				warnings=$((warnings + 1))
				printf "! WARNING: %s HAS NO TEST CASES\\n" "$testscript"
			fi
			testvar_save warnings
			testvar_save testcases
		) || true
	done < $T/test-scripts
}

print_summary () {
	testvar_load testcases
	testvar_load failures
	testvar_load warnings
	testvar_load errors
	
	[[ $testcases -eq 0 ]] && {
		echo
		echo "No tests to run!"
		exit 1
	}
	
	echo "Ran: $testcases; Passed: $((testcases - ( failures + errors ) )); Failed: $failures; Warnings: $warnings; Errors: $errors"
	
	[[ $((failures + warnings + errors)) -gt 0 ]] && {
		echo
		[[ $warnings -gt 0 ]] && echo "There are $warnings warnings"
		[[ $failures -gt 0 ]] && echo "There are $failures test failures"
		[[ $errors   -gt 0 ]] && echo "There are $errors test errors"
		exit 1
	}
}

list_tests "$@"
runtests 2>&1 | tee -a target/testresults/results.txt
print_summary | tee -a target/testresults/results.txt
