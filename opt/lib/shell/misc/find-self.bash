#! /bin/bash
#
# Finds the location of the "main" script through symlinks.
# This must be sourced from the top level script.
# It prints out the absolute path location of the main script.
#
# This works by continually resolving symbolic links
# to the executing script until the actual script is located.
#
# This uses ${BASH_SOURCE[1]} which is why it must
# be sourced from the top level script.
#

(
	SOURCE="${BASH_SOURCE[1]}"
	while [ -h "$SOURCE" ]; do
		LAST_SOURCE="$SOURCE"
		SOURCE="$(readlink "$SOURCE")"
		if [ "${SOURCE:0:1}" != / ]; then
			SOURCE="$(dirname "$LAST_SOURCE")/$SOURCE"
		fi
	done
	echo "$SOURCE"
)
