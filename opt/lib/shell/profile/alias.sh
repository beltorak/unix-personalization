#!/bin/bash

if test "x$BASH_SOURCE" != "x"; then
	_DIR=/etc/profile.d/alias.d
	[[ -d $_DIR ]] && {
		for _file in $_DIR/*; do
			_file="$(basename "$_file")"
			# format of valid file is NNNN-pkgname-command
		 	[[ ! -z "$(echo "$_file" | grep -iE '^[0-9]+(-[a-z][a-z0-9_.-]*)*')" ]] &&	[[ -x $_DIR/$_file ]] && {
				alias $(echo $_file | cut -f 3- -d -)="$(< $_DIR/$_file)"
			}
		done
	}
	unset _DIR
	unset _file
fi
