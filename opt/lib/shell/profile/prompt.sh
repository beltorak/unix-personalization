#!/bin/bash
# Filename:	  custom_prompt.sh
# original script from http://www.debian-administration.org/articles/205
# Maintainer:	Dave Vehrs 
# Last Modified: 12 Jul 2005 13:29:40 by Dave Vehrs

if test "x$BASH_SOURCE" != "x"; then
	_PROMPTCMD_CONF_DEFAULT="$(dirname "$(readlink -f "$BASH_SOURCE")")/prompt.conf"
	if [ "$(dirname "$BASH_SOURCE")" = '/etc/profile.d' -a -r /etc/profile.d/prompt.conf ]; then
		_PROMPTCMD_CONF=/etc/profile.d/prompt.conf
	else
		_PROMPTCMD_CONF="$_PROMPTCMD_CONF_DEFAULT"
	fi
	
	. "$(readlink -f "${BASH_SOURCE}").functions"
	load_prompt
fi
