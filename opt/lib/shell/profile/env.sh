#!/bin/bash

if test "x$BASH_SOURCE" != "x"; then
	_DIR=/etc/profile.d/env.d
	[[ -d $_DIR ]] && {
		for _file in $_DIR/*; do
			_file="$(basename "$_file")"
			# format of valid _file is NNNN-pkgname-ENV_NAME
			[[ ! -z "$(echo "$_file" | grep -iE '^[0-9]+(-[a-z][a-z0-9_.-]*)*')" ]] && [[ -x $_DIR/$_file ]] && {
				_name=$(echo $_file | cut -f 3- -d -)
				_value="$(< $_DIR/$_file)"
				if [ "${_value:0:2}" = "~/" ]; then
					_value="${HOME}${_value:1}"
				fi
				export $_name="$_value"
			}
		done
	}
	unset _DIR
	unset _file
	unset _name
	unset _value
fi

