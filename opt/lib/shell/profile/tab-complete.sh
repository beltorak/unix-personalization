#!/bin/sh
if test -n "$PS1"; then
	bind 'set match-hidden-files off'
	bind 'set completion-ignore-case on'
fi
