#!/bin/bash

if test "x$BASH_SOURCE" != "x"; then
	_SRC_DIR=$(dirname $(readlink -f ${BASH_SOURCE[0]}))
	if [  "$_SRC_DIR" ]; then
		. $_SRC_DIR/path.sh.functions
		path_proc_dir /etc/profile.d/path.d
	fi
	unset _SRC_DIR
	unset path_proc_dir
fi

