#!/bin/sh

set_java_home () {
	_JAVA_PATH="$(readlink -f "$1")"
	if [ ! -d "$_JAVA_PATH" -o ! -x "$_JAVA_PATH/bin/java" ]; then
		echo "Bad Java Path: $1"
		return 1
	fi

	if [ -n "${JAVA_HOME:-}" ]; then
		path_remove "$JAVA_HOME"
		path_remove "$JAVA_HOME/jre"
	fi

	JAVA_HOME="$_JAVA_PATH"
	export JAVA_HOME
	path_insert "$JAVA_HOME/bin"
	if [ -x "$_JAVA_PATH/jre/bin/java" ]; then
		path_insert "$JAVA_HOME/jre/bin"
	fi
	export PATH
}

