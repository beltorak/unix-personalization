shunit () {
	if [ -z "${SHUNIT2_RUNNER:-}" ]; then
		echo "Need env var SHUNIT2_RUNNER to point to shunit2 script" >&2
		return 1
	fi
	if [ -z "${1:-}" ]; then
		echo "Need first argument as script to test" >&2
		return 1
	fi
	if [ ! -r "$1" ]; then 
		echo "$1 is not a readable file" >&2
		return 1
	fi
	if [ ! -d "${2:-tests}" ]; then
		echo "Need either 'tests' directory or second argument pointing to tests directory" >&2
		return 1
	fi

	find "${2:-tests}" -type f -name '*-tests.sh' -print0 | xargs -n1 -0 env TESTED_SCRIPT="`readlink -f "$1"`"
}
