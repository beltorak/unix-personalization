#!/bin/bash

function help_promptcmd () {
	sed -n -e '/^#[^#]/d;/^\s*$/d;s/^##/\n##/;p' "$_PROMPTCMD_CONF"
}

help_promptcmd_colors () {
	local style
	local fore
	local back

	promptcmd_define_colors

	for style in ${_PROMPTCMD_COLORSTYLES[@]}; do
	for fore in ${_PROMPTCMD_COLORS[@]}; do
	for back in ${_PROMPTCMD_COLORS[@]}; do
		local color="$fore:$back+$style"
		printf '\x1b[%s %-19s \x1b[0m' "$(promptcmd_parse "$color")" "${color/+default/}"
	done; echo
	done; echo
	done
}


# Determine what prompt to display:
# 1.  Display simple custom prompt for shell sessions started
#	 by script.  
# 2.  Display "bland" prompt for shell sessions within emacs or 
#	 xemacs.
# 3   Display promptcmd for all other cases.

function load_prompt () {
	# Get PIDs
	local parent_process my_process unused
	IFS= read -r -d '' parent_process unused < /proc/$PPID/cmdline
	IFS= read -r -d '' my_process < /proc/$$/cmdline

	if  [[ $parent_process == script* ]]; then
		PROMPT_COMMAND=""
		PS1="\t - \# - \u@\H { \w }\$ "
	elif [[ $parent_process == emacs* || $parent_process == xemacs* ]]; then
		PROMPT_COMMAND=""
		PS1="\u@\h { \w }\$ "
	else
		export DAY=$(date +%A)
		PROMPT_COMMAND=promptcmd
	fi 
	export PS1 PROMPT_COMMAND
}

# $1 - fg[:bg][+style]
# stdout: escape color sequence without leading ESC[
promptcmd_parse () {
	local spec="$1"

	if [ ! "${_PROMPTCMD_COLORS:-}" ]; then
		promptcmd_define_colors
	fi

	if [ "${spec//+/}" != "$spec" ]; then
		local style="_PROMPTCMD_COLORSTYLE_${spec#*+}"
		if [ ! "${!style:-}" ]; then
			echo "Bad style in color spec: $1" >&2
			style=
		fi
		spec="${spec%+*}"
	fi
	if [ "${spec//:/}" != "$spec" ]; then
		local back="_PROMPTCMD_COLOR_${spec#*:}"
		if [ ! "${!back:-}" ]; then
			echo "Bad background in color spec: $1" >&2
			back=
		fi
		spec="${spec%:*}"
	fi
	local fore="_PROMPTCMD_COLOR_$spec"
	if [ ! "${!fore:-}" ]; then
		echo "Bad forground in color spec: $1" >&2
		fore=
	fi

	if [ "${style:-}${fore:-}${back:-}" ]; then
		echo "${style:+${!style};}3${!fore:-$_PROMPTCMD_COLOR_default}${back:+;4${!back}}m"
	fi
}

# defines color primitives in the current environment
promptcmd_define_colors () {
	[[ "$_PROMPTCMD_COLORSTYLES" ]] && return
	_PROMPTCMD_COLORSTYLES=( default bold faint emph under blink )
	_PROMPTCMD_COLORS=( black red green brown blue purple cyan gray )
	local style
	local num=0
	for style in ${_PROMPTCMD_COLORSTYLES[@]}; do
		printf -v _PROMPTCMD_COLORSTYLE_${style} $((num++))
	done
	local color
	local num=0
	for color in ${_PROMPTCMD_COLORS[@]}; do
		printf -v _PROMPTCMD_COLOR_${color} $((num++))
	done
}

promptcmd_load_conf () {
	local last_mod=$(stat --format "%Y" "$_PROMPTCMD_CONF")
	[[ $last_mod -gt ${_PROMPTCMD_LAST_DEFINED:-0} ]] || return 1
	local symcolor
	local def
	local colorvar
	while read symcolor def; do
		printf -v symcolor '_PROMPTCMD_CONF_%s' "$(echo "$symcolor" | tr '[[:lower:]]-' '[[:upper:]]_')"
		case "$symcolor" in
			_PROMPTCMD_CONF_ADMIN_GROUPS)
				printf -v ${symcolor} '%s' "$def"
			;;
			*)
				printf -v ${symcolor} '%s' "$(promptcmd_parse "$def")"
			;;
		esac
	done < <( grep -E -v '^\s*$|^#' "$_PROMPTCMD_CONF" )
	_PROMPTCMD_LAST_DEFINED=$last_mod
	return 0
}

promptcmd_define_ps1_parts () {
	local -n var_ref="$1"
	shift
	
	printf -v ${!var_ref}[TTY] "%s" $(tty | sed -e 's,.*/dev/,,')
	if [[ ${var_ref[TTY]} ]]; then 
		printf -v ${!var_ref}[SESS_SRC] "%s" $(who | grep "$TTY "  | (read _ _ _ _ _ tty; echo "$tty"))
	fi
	
	if [[ ${SSH_CLIENT} ]] || [[ ${SSH2_CLIENT} ]]; then 
		printf -v ${!var_ref}[SSH_FLAG] "%s" "1"
	fi
	if [ ${var_ref[SSH_FLAG]:-0} -eq 1 ]; then 
		printf -v ${!var_ref}[_LOGIN] "%s" "${_PROMPTCMD_CONF_LOGIN_REMOTE_SSH}"
	elif [[ -n "${var_ref[SESS_SRC]}" ]]; then 
		if [ "${var_ref[SESS_SRC]}" == "(:0.0)" ]; then 
			printf -v ${!var_ref}[_LOGIN] "%s" "${_PROMPTCMD_CONF_LOGIN_LOCAL}"
		else
			local parent_process=`cat /proc/${PPID}/cmdline`
			if [[ "$parent_process" == "in.rlogind*" ]]; then
				printf -v ${!var_ref}[_LOGIN] "%s" "${_PROMPTCMD_CONF_LOGIN_REMOTE_RSH}"
			elif [[ "$parent_process" == "in.telnetd*" ]]; then 
				printf -v ${!var_ref}[_LOGIN] "%s" "${_PROMPTCMD_CONF_LOGIN_REMOTE_TELNET}"
			else
				printf -v ${!var_ref}[_LOGIN] "%s" "${_PROMPTCMD_CONF_LOGIN_REMOTE_UNKNOWN}"
			fi
		fi
	elif [[ -z "${var_ref[SESS_SRC]}" ]]; then
		printf -v ${!var_ref}[_LOGIN] "%s" "${_PROMPTCMD_CONF_LOGIN_LOCAL}"
	else
		printf -v ${!var_ref}[_LOGIN] "%s" "${_PROMPTCMD_CONF_LOGIN_REMOTE_UNKNOWN}"
	fi

	if [ -z "$http_proxy" ] ; then
		printf -v ${!var_ref}[_HTTP_PROXY] "%s" "${_PROMPTCMD_CONF_HTTP_PROXY_OFF}"
	else
		printf -v ${!var_ref}[_HTTP_PROXY] "%s" "${_PROMPTCMD_CONF_HTTP_PROXY_ON}"
	fi

	if [ -w "${PWD}" ]; then 
		printf -v ${!var_ref}[_CWD] "%s" "${_PROMPTCMD_CONF_CWD_RW}"
	else
		printf -v ${!var_ref}[_CWD] "%s" "${_PROMPTCMD_CONF_CWD_RO}"
	fi

	if [ ${UID} -eq 0 ] ; then
		if [ "${USER}" == "${LOGNAME}" ]; then
			if [[ ${SUDO_USER} ]]; then
				printf -v ${!var_ref}[_USER] "%s" "${_PROMPTCMD_CONF_USER_SUDO_0}"
			else
				printf -v ${!var_ref}[_USER] "%s" "${_PROMPTCMD_CONF_USER_LOGIN_0}"
			fi
		else
			printf -v ${!var_ref}[_USER] "%s" "${_PROMPTCMD_CONF_USER_SU_0}"
		fi
	else
		if [ ${USER} == "${LOGNAME}" ]; then
			if [ ! -z "$(groups | grep -Ew "${_PROMPTCMD_CONF_ADMIN_GROUPS// /|}")" ]; then
				printf -v ${!var_ref}[_USER] "%s" "${_PROMPTCMD_CONF_USER_ADMIN}"
			else
				printf -v ${!var_ref}[_USER] "%s" "${_PROMPTCMD_CONF_USER_DEFAULT}"
			fi
		else
			printf -v ${!var_ref}[_USER] "%s" "${_PROMPTCMD_CONF_USER_SU_OTHER}"
		fi
	fi
	
}

# Function to set prompt_command to.
function promptcmd () {
	# Color "primitives"
	promptcmd_define_colors
	local -A _promptcmd_ps1_parts
	
	promptcmd_load_conf
	promptcmd_define_ps1_parts _promptcmd_ps1_parts
	
	history -a
	
	# Test for day change.
	if [ -z $DAY ] ; then
		export DAY=$(date +%A)
	else
		local today=$(date +%A)
		if [ "${DAY}" != "${today}" ]; then
			PS1="${PS1}\n\[\e[${_PROMPTCMD_CONF_DAYCHANGE}\]Day changed to $(date '+%A, %d %B %Y').\n"
			export DAY=$today
		fi
	fi

	PS1='\n'
	PS1="${PS1}\[\e[${_promptcmd_ps1_parts[_USER]}\]\u\[\e[0m\] "
	PS1="${PS1}\[\e[${_promptcmd_ps1_parts[_HTTP_PROXY]}\]@\[\e[0m\] "
	PS1="${PS1}\[\e[${_promptcmd_ps1_parts[_LOGIN]}\]\h\[\e[0m\] "
	PS1="${PS1}\[\e[${_promptcmd_ps1_parts[_USER]}\][\[\e[0m\]\[\e[${_promptcmd_ps1_parts[_CWD]}\]\w\[\e[${_promptcmd_ps1_parts[_USER]}\]]\[\e[0m\]\\n"
	PS1="${PS1}\\$\[\e[0m\] "
}

