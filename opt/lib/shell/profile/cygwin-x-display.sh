#!/bin/bash

DISPLAY=$(find /tmp/.X11-unix -type s -name X\* -user "$USER" | xargs -L 1 basename | sed -e 's/^X//' | sort -gr | head -n 1)
if [ "$DISPLAY" ]; then
	export DISPLAY=:$DISPLAY
else
	unset DISPLAY
fi
