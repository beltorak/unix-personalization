#!/bin/bash

test_abs_relative () {
	cd /tmp
	_path_abs paths "/" "" "/test" "test"
	expected_paths=( / /test /tmp/test )
	assert_var_equals "Paths" expected_paths paths
}

test_abs_homes () {
	HOME=/var
	cd /tmp
	_path_abs paths "~" "~/" "~/stuff" "/stuff/~" "stuff/~done"
	expected_paths=( /var /var /var/stuff "/stuff/~" "/tmp/stuff/~done" )
	assert_var_equals "Paths" expected_paths paths
}

