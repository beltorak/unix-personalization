#!/bin/bash

dump_env () {
	declare | grep -Ei "^[a-z0-9_]*=" | grep -vE '^_=|^BASH_LINENO=' > "$T/${TEST_NAME}.env.$1"
}

test_no_env_pollution () {
	dump_env before
	path_remove /var/goobly
	dump_env after
	env_diff="$(diff -u "$T/${TEST_NAME}.env.before" "$T/${TEST_NAME}.env.after")"
	assert_equals "Env Diff" "" "$env_diff"
}

test_remove_relative () {
	PATH="/usr/local/bin:/usr/bin:/bin:/tmp/bin"
	cd /tmp
	path_remove bin
	expected_path="/usr/local/bin:/usr/bin:/bin"
	assert_var_equals "" expected_path PATH
}
