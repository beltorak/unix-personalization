#!/bin/bash

dump_env () {
	declare | grep -Ei "^[a-z0-9_]*=" | grep -vE '^PATH=|^_=|^BASH_LINENO=' > "$T/${TEST_NAME}.env.$1"
}

test_no_env_pollution () {
	dump_env before
	path_insert /var/goobly
	dump_env after
	env_diff="$(diff -u "$T/${TEST_NAME}.env.before" "$T/${TEST_NAME}.env.after")"
	assert_equals "Env Diff" "" "$env_diff"
}

test_insert_path_items () {
	PATH="/usr/local/bin:/usr/bin:/bin:/opt/bin:/usr/bin:/sbin"
	path_insert /usr/bin /blar
	expected_path="/usr/bin:/blar:/usr/local/bin:/bin:/opt/bin:/sbin"
	assert_var_equals "PATH" expected_path PATH
}

test_insert_relative () {
	PATH="/usr/local/bin:/usr/bin:/bin:/opt/bin:/usr/sbin:/sbin"
	cd /tmp
	path_insert . bar ./baz
	expected_path="/tmp:/tmp/bar:/tmp/baz:/usr/local/bin:/usr/bin:/bin:/opt/bin:/usr/sbin:/sbin"
	assert_var_equals "PATH" expected_path PATH
}

test_insert_twice () {
	PATH="/usr/local/bin:/usr/bin:/bin:/opt/bin:/usr/sbin:/sbin"
	path_insert /wib
	path_insert /wib
	expected_path="/wib:/usr/local/bin:/usr/bin:/bin:/opt/bin:/usr/sbin:/sbin"
	assert_var_equals "PATH" expected_path PATH
}

test_insert_relative_twice () {
	PATH="/usr/local/bin:/usr/bin:/bin:/opt/bin:/usr/sbin:/sbin"
	cd /tmp
	path_insert wib
	path_insert wib
	expected_path="/tmp/wib:/usr/local/bin:/usr/bin:/bin:/opt/bin:/usr/sbin:/sbin"
	assert_var_equals "PATH" expected_path PATH
}
