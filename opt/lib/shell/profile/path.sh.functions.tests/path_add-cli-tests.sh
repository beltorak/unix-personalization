#!/bin/bash

dump_env () {
	declare | grep -Ei "^[a-z0-9_]*=" | grep -vE '^PATH=|^_=|^BASH_LINENO=' > "$T/${TEST_NAME}.env.$1"
}

test_no_env_pollution () {
	dump_env before
	path_add /var/goobly
	dump_env after
	env_diff="$(diff -u "$T/${TEST_NAME}.env.before" "$T/${TEST_NAME}.env.after")"
	assert_equals "Env Diff" "" "$env_diff"
}

test_add_path_items () {
	PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/opt/bin:/usr/local/games:/sbin:/usr/games"
	orig_path="$PATH"
	expected_path="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/bin:/opt/bin:/usr/local/games:/usr/games:/goobly:/gobbly:/sbin"
	path_add /goobly /gobbly /sbin
	assert_var_equals "Paths" expected_path PATH
}

test_add_relative () {
	PATH="/usr/local/bin:/usr/bin:/bin:/opt/bin:/usr/sbin:/sbin"
	cd /tmp
	path_add . bar ./baz
	expected_path="/usr/local/bin:/usr/bin:/bin:/opt/bin:/usr/sbin:/sbin:/tmp:/tmp/bar:/tmp/baz"
	assert_var_equals "PATH" expected_path PATH
}

test_add_double () {
	# FIXME: this fails because it does a straight concat of new paths to array. It should add the new paths, but strip duplicates.
	PATH="/usr/local/bin:/usr/bin:/bin:/opt/bin:/usr/sbin:/sbin"
	expected_path="$PATH:/tmp"
	path_add /tmp /tmp
	assert_var_equals "PATH" expected_path PATH
}
