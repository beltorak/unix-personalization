#!/bin/bash

test_one_item () {
	_to_path_items path_items "/one"
	expected_items=( "/one" )
	assert_var_equals "" expected_items path_items
}

test_two_items () {
	_to_path_items itemized "/one:/two"
	expected_items=( "/one" "/two" )
	assert_var_equals "" expected_items itemized
}

test_no_items () {
	_to_path_items itemized
	assert_equals "var repr" "declare -a itemized='()'" "$(declare -p itemized)"
	assert_equals "var count" 0 ${#itemized[@]}
}
