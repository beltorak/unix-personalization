#!/bin/bash

before_each () {
	path_array=( /usr/local/sbin /usr/local/bin /usr/sbin /usr/bin /sbin /sbin /opt/bin /usr/local/games /usr/games )
	orig_path_array=( "${path_array[@]}" )
}

test_remove_no_givin_items () {
	_path_remove path_array
	assert_var_equals "Path Arrays" orig_path_array path_array
}

test_remove_item_not_in_array () {
	_path_remove path_array /barg/nobbit
	assert_var_equals "Path Arrays" orig_path_array path_array
}

test_remove_item () {
	_path_remove path_array /usr/sbin
	expected_array=( /usr/local/sbin /usr/local/bin /usr/bin /sbin /sbin /opt/bin /usr/local/games /usr/games )
	assert_var_equals "Path Arrays" expected_array path_array
}

test_remove_multi () {
	_path_remove path_array /usr/games /opt/bin
	expected_array=( /usr/local/sbin /usr/local/bin /usr/sbin /usr/bin /sbin /sbin /usr/local/games )
	assert_var_equals "Path Arrays" expected_array path_array
}

test_remove_copies () {
	_path_remove path_array /sbin
	expected_array=( /usr/local/sbin /usr/local/bin /usr/sbin /usr/bin /opt/bin /usr/local/games /usr/games )
	assert_var_equals "Path Arrays" expected_array path_array
}
