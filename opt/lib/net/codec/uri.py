#!/usr/bin/env python3

import argparse
import collections
import functools
import os.path
import sys
import urllib.parse

def _main():
	"""
	Prints the arguments uri-encoded or uri-decoded.

	When invoked as 'uri-encode', same as the option '-e'.
	When invoked as 'uri-decode', same as the option '-d'.
	"""

	opts = _parse_args(sys.argv[1:], _infer_mode(os.path.basename(sys.argv[0])))
	applyCoder = functools.partial(_apply_coder, coder=opts.coder)
	joiner = opts.coder(" ") if opts.isOneLine else "\n"
	print(joiner.join(applyCoder(opts, words=opts.words if opts.words else _lines(sys.stdin))))
#

def _apply_coder(opts, coder, words):
	for word in words:
		yield coder(opts, word)
#

def _lines(fileObj):
	for line in fileObj:
		yield line.rstrip('\r\n')
#

def _parse_args(args, mode = None):
	"""
	Parses command line arguments.

	The mode is the implied mode denoted by the name this program was invoked as.
	"""
	argp = argparse.ArgumentParser(description=_main.__doc__)
	if not mode.isImplied:
		modeArgs = argp.add_mutually_exclusive_group(required=True)
		modeArgs.add_argument('-e','--encode',action='store_const',const=uriEncode,dest='coder',help="URI Encode")
		modeArgs.add_argument('-d','--decode',action='store_const',const=uriDecode,dest='coder',help="URI Decode")
	argp.add_argument('-1','--one-line',action='store_const',const=True,default=False,dest='isOneLine',help="Keep output on single line.")
	argp.add_argument('--safe-chars', dest="safeChars", default="", help="Don't encode these chars; default is to encode everything not allowed.")
	argp.add_argument('words',nargs="*",help="The words to encode or decode. If absent, use stdin.")

	parsed = argp.parse_args(args)
	if mode.isImplied:
		parsed.coder = mode.coder
	return parsed
#

def _infer_mode(programName):
	ImpliedMode = collections.namedtuple('ImpliedMode', ['coder', 'programName', 'isImplied'])
	if programName == 'uri-encode':
		return ImpliedMode(coder=uriEncode, programName=programName, isImplied=True)
	if programName == 'uri-decode':
		return ImpliedMode(coder=uriDecode, programName=programName, isImplied=True)
	return ImpliedMode(coder=NotImplementedError, programName=programName, isImplied=False)
#

def uriEncode(opts, word):
	return urllib.parse.quote(word,safe=(opts.safeChars and [opts.safeChars] or [""])[0])
#

def uriDecode(opts, word):
	return urllib.parse.unquote(word)
#

if __name__ == '__main__':
	_main()

