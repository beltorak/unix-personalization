set_proxy () {
	if [ -z "${1:-}" ]; then
		unset http_proxy
		return
	fi
	declare name url in_file
	in_file=0
	while read name url; do
		case "${name:-}" in
			"") true ;;
			"#"*) true ;;
			*)
				if [ "$1" = "$name" ]; then
					export http_proxy="$url"
					in_file=1
					break
				fi;
			;;
		esac
	done < "${HTTP_PROXY_LIST_FILE:-/etc/stdproxies}"
	if [ $in_file -eq 0 ]; then
		export http_proxy="$1"
	fi
}
