#! /bin/bash

function cat_template_head () {
	cat<<-"__EOF__"
		Section: misc
		Priority: optional
		Standards-Version: 3.6.2
		
		Package: mononono
		Maintainer: Trevor "beltorak" Torrez <trevor.torrez@beltorak.info>
	__EOF__
}

function cat_conflicts () {
	echo "Conflicts: $(cat "$1" | sort -u | sed -e '/^\s*$/ d ; /^#/ d' | sed -n -e '$ n ; s/$/,/p' | tr '\n' ' ')"
}

function cat_template_tail () {
	cat<<-"__EOF__"
		Architecture: all
		Copyright: /usr/share/common-licenses/BSD
		Description: Introduces an intentional conflict with Mono packages. 
		 By creating an intentional conflict with mono packages, this 
		 package can be installed to prevent Mono from being installed 
		 (or at least forcing you to address the conflict).
	__EOF__
}

main () {
	local CWD=$(dirname $(readlink -f "${BASH_SOURCE[0]}"))
	mkdir -p "$CWD/work"
	touch $CWD/package-list.txt
	cat_template_head > $CWD/work/mononono.ctl
	cat_conflicts "$CWD/package-list.txt" >> $CWD/work/mononono.ctl
	cat_template_tail >> $CWD/work/mononono.ctl

	( cd $CWD/work; equivs-build mononono.ctl )
}

main

