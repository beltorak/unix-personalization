#!/bin/bash

test_convert_lowercase_text () {
	sc_convert_text s2 "whan"
	converted="${sc_retval:-not_set}"
	assertEquals '"whan" converted to Set 2 scan codes' "1D F0 1D 33 F0 33 1C F0 1C 31 F0 31" "$converted"
}

test_convert_uppercase_text () {
	sc_convert_text s2 "WHAN"
	converted="${sc_retval:-not_set}"
	assertEquals '"WHAN" converted to Set 2 scan codes' "12 1D F0 1D 33 F0 33 1C F0 1C 31 F0 31 F0 12" "$converted"
}

test_convert_multiword_mixed_case () {
	sc_convert_text s2 "Whan" "thAT" "aPRil"
	converted="${sc_retval:-not_set}"
	
	words=()
	words+=( "12 1D F0 1D F0 12 33 F0 33 1C F0 1C 31 F0 31" )
	words+=( "2C F0 2C 33 F0 33 12 1C F0 1C 2C F0 2C F0 12" )
	words+=( "1C F0 1C 12 4D F0 4D 2D F0 2D F0 12 43 F0 43 4B F0 4B" )
	assertEquals '"Whan" "thAT" "aPRil" converted to Set 2 scan codes' "${words[*]}" "$converted"
}

test_convert_mixed_case_text_with_spaces () {
	sc_convert_text s2 "Whan thAT aPRil"
	converted="${sc_retval:-not_set}"
	
	words=()
	words+=( "12 1D F0 1D F0 12 33 F0 33 1C F0 1C 31 F0 31" )
	words+=( "29 F0 29" )
	words+=( "2C F0 2C 33 F0 33 12 1C F0 1C 2C F0 2C" )
	words+=( "29 F0 29" )
	words+=( "F0 12" )
	words+=( "1C F0 1C 12 4D F0 4D 2D F0 2D F0 12 43 F0 43 4B F0 4B" )
	assertEquals '"Whan thAT aPRil" converted to Set 2 scan codes' "${words[*]}" "$converted"
}

test_convert_mixed_punctuation () {
	text='l(o) W(!) vi`iv~'
	sc_convert_text s2 "$text"
	converted="${sc_retval:-not_set}"

	words=()
	words+=( 4B F0 4B 12 46 F0 46 F0 12 44 F0 44 12 45 F0 45 )
	words+=( 29 F0 29 )
	words+=( 1D F0 1D 46 F0 46 16 F0 16 45 F0 45 )
	words+=( 29 F0 29 )
	words+=( F0 12 2A F0 2A 43 F0 43 0E F0 0E 43 F0 43 2A F0 2A 12 0E F0 0E F0 12 )
	assertEquals '"'"$text"'" converted to Set 2 scan codes' "${words[*]}" "$converted"
}

oneTimeSetUp () {
	SC_IS_SOURCING=1
	. "${TESTED_SCRIPT}"
}

. "${SHUNIT2_RUNNER}"

