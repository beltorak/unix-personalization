#!/bin/bash

test_parse_single_press () {
	sc_parse_seq "[lshift]"
	actual="$(declare -p sc_retval)"

	sc_retval=( "press lshift" )
	expected="$(declare -p sc_retval)"

	assertEquals "$expected" "$actual"
}

test_parse_multi_press () {
	sc_parse_seq "{[}[enter]"
	actual="$(declare -p sc_retval)"

	sc_retval=( "press [" "press enter" )
	expected="$(declare -p sc_retval)"

	assertEquals "$expected" "$actual"
}

test_parse_raw_text () {
	sc_parse_seq "I am the lizard king"
	actual="$(declare -p sc_retval)"

	sc_retval=( "text I am the lizard king" )
	expected="$(declare -p sc_retval)"

	assertEquals "$expected" "$actual"
}

test_parse_trailing_text () {
	sc_parse_seq "[capslock]:'; drop table users; --"
	actual="$(declare -p sc_retval)"

	sc_retval=( "press capslock" "text '; drop table users; --" )
	expected="$(declare -p sc_retval)"

	assertEquals "$expected" "$actual"
}

test_trailing_text_confusable_as_seq () {
	sc_parse_seq "[capslock]:[capslock]"
	actual="$(declare -p sc_retval)"

	sc_retval=( "press capslock" "text [capslock]" )
	expected="$(declare -p sc_retval)"

	assertEquals "$expected" "$actual"
}

test_parse_depress () {
	sc_parse_seq "+[w][a][d]"
	actual="$(declare -p sc_retval)"

	sc_retval=( "depress w" "press a" "press d" )
	expected="$(declare -p sc_retval)"

	assertEquals "$expected" "$actual"
}

test_key_repitition () {
	sc_parse_seq "+[backspace][kp-8]x3[enter]"
	actual="$(declare -p sc_retval)"

	sc_retval=( "depress backspace" "press kp-8" "press kp-8" "press kp-8" "press enter" )
	expected="$(declare -p sc_retval)"

	assertEquals "$expected" "$actual"
}

test_hold_keys () {
	sc_parse_seq "=[lshift]=[lctrl]:whan that april"
	actual="$(declare -p sc_retval)"

	sc_retval=( "hold lshift" "hold lctrl" "text whan that april" "unhold lctrl" "unhold lshift" )
	expected="$(declare -p sc_retval)"

	assertEquals "$expected" "$actual"
}

oneTimeSetUp () {
	SC_IS_SOURCING=1
	. "${TESTED_SCRIPT}"
}

. "${SHUNIT2_RUNNER}"

