#!/bin/bash

if [ -z "${SC_IS_SOURCING:-}" ]; then
	set -o errexit
	set -o nounset
	set -o pipefail
fi

SCRIPT_SELF="$(readlink -f "$BASH_SOURCE")"
SCRIPT_NAME="$(basename "${0:-${SCRIPT_SELF}}")"
SCRIPT_HOME="$(dirname "$SCRIPT_SELF")"

sc_cat_help () {
	cat<<__EOF__ | sed -e 's/^\t\t//;s/\t/    /g'
		Usage: $SCRIPT_NAME mode [--] sequence sequence sequence ...

		This script translates sequences to keyboard make and break codes.

		The mode must be one of the scancode sets - 1, 1x, 2, 2x, 3, 3x, or usb.
		After that one or more sequences of text or actions on individual keys.

		A key is named by putting it in square or curly brackets, and may
		optionally be prefixed with a modifier ("+" to depress the key, "-" to
		release, "=" to hold until the end of the sequence, and no prefix
		is a press - a depress followed by a release). Additionally, raw ascii
		text can be entered as a sequence and may be prefixed with ":".
		Text can also end a sequence if prefixed with ":".

		Some examples:
			$SCRIPT_NAME 1 "=[lctrl]=[lalt][del]" # the Three Finger Salute
			$SCRIPT_NAME 1 "=[lshift]:totally not robots" "[enter]"
		Note in that last one, the 'lshift' key is released before the
		'enter' key is pressed.
			$SCRIPT_NAME 1 ":[lctrl]" # The literal text '[lctrl]'.
			$SCRIPT_NAME 1 "I have never" "[backspace]x5" " ... sometimes"
__EOF__
}

############### Input BNF (Proposed)
# seq ::= key-seq | text-string
# key-seq ::= key | key key-seq | text-flag | text-flag text-string
# key ::= named-key | named-key repitition | action named-key
# repitition ::= 'x' decimal-integer
# action ::= '+' | '-' | '='
#   # + depress (hold)
#   # - release
#   # = depress until end of seq
#   #   Lack of an action is a press (depress followed immediately by a release)
# named-key ::= '[' key-name ']' | '{' key-name '}'
# text-flag ::= ':'
#   # Indicates the rest of the seq is ascii text characters;
#   # needed as an "escape" for ascii starting with an action or text-flag
# 
# Examples:
#   Three Finger Salute: "=[lctrl]=[lalt][del]"
#       Alternately: "+[lctrl]+[lalt][del]-[lalt]-[lctrl]"
#                    "+{LCTRL}+{LALT}[del]-[lalt]-[lctrl]"
#       The key-name is case insensitive, and the variation of '[]' vs '{}' allows
#       combining with the action more easily.
#   Auto Shouting: "[capslock]:i don't know what i am yelling about"
#   Determined Shouting: "=[lshift]:I GET IT NOW"
#   Natural Shouting: "I AM THE LIZARD KING"
#       The difference between shift, capslock, and simply using uppercase letters
#       is that the text parser will automatically modulate the shift key
#       to account for letter case changes. So "=[lshift]:I am" effectively becomes
#       "+[lshift][i][space]-[lshift][a][m]". (Note it is assumed that the space
#       is not affected by shift modifiers; no attempt is made to modulate
#       any other modifiers or modulate the shift key "around" the text spaces.)
#       Also note, the text parser's shift key modulation will remain
#       blissfully unaware of the capslock state, effectively simulating an
#       aLL tOO cOMMON hUMAN eRROR.
#       
#   Frantic Backpeddling: "I have never" "[backspace]x5:... sometimes."
#   Roguelike Gaming: "[up]x2[down]x3[up][left]x4"
#
#   The difference between depressing a key and holding a key until the end of a seq
#   is what happens to the next seq. "=[lctrl]:ad" "be" will emit bytes to release
#   the CTRL before the scancodes of the second seq, whereas "+{LCTRL}:ad" "be"
#   will not.
#

# This global variable allows passing non-integer return values from functions.
declare -a sc_retval

sc_get_raw_token () {
	printf -v sc_retval "%s" $(printf "%s" "$1" | hexdump -ve '/1 "%02x"')
}
sc_get_token () {
	printf -v sc_retval "%s" $(printf "%s" "${1,,}" | hexdump -ve '/1 "%02x"')
}

# This routine declares global associative arrays of the form 'sc_<token>' where the token is
# the lowercased version of the key (letter, number, punctuation, ...) or the key description
# (lshift, rctrl, ...). The keys of the associative arrays are the scan code modes (usb, 
# s1, s1x, s2, s2x, s3, s3x) and the value is a space separated string of hex bytes.
#
# The first argument is an identifier for the key/case, and is pretty much ignored but helpful in `set -x` contexts.
# The second argument is the token.
# The remaining arguments are the scan code byte sequences for each mode in the order listed above.
sc_set_scan_code () {
	local id="$1"
	shift

	local token
	token="$1"
	shift

	declare -g -A sc_$token
	printf -v sc_$token['key'] "%s" "$id"
	printf -v sc_$token['usb'] "%02X" "$1"
	shift
	
	declare m
	for m in s1 s1x s2 s2x s3 s3x; do
		printf -v sc_$token["$m"] "%02X " 0x${1//-/ 0x}
		shift
	done
}
# The table of keys to scan codes is created by sc_set_scan_code(), but this function populates
# an important set of global variables of the form 'sc_is_shiftpunct_<token>' and
# 'sc_is_regpunct_<token>' are used in manually that is set to 0 or 1 depending on if the
# punctuation character requires a shift modifier to access. For the letters there exists 
# 'sc_is_ucase_<token>' and 'sc_is_lcase_<token>' that are likewise set to 0 or 1.
#
# Also note that the keypad tokens are always preceeded with 'kp-' to distinguish some of the
# keys.
parse_scan_code_table () {
	# DEBUG echo "Parsing Scan Code Table"
	local _case _id _sc_usb _sc_1 _sc_1x _sc_2 _sc_2x _sc_3 _sc_3x _sc_key _k
	while IFS=$'\t' read -r _case _id _sc_usb _sc_1 _sc_1x _sc_2 _sc_2x _sc_3 _sc_3x _sc_key; do
		case $_case in
			KPALT)
				for _k in ${_sc_key}; do
					sc_get_token "kp-${_k/kp-/}"
					[[ "$_k" = "/" ]] || sc_set_scan_code "$_id($_case):$_sc_key" "$sc_retval" $_sc_usb $_sc_1 $_sc_1x $_sc_2 $_sc_2x $_sc_3 $_sc_3x
				done
			;;
			USBALT)
				for _k in ${_sc_key}; do
					sc_get_token "$_k"
					sc_set_scan_code "$_id($_case):$_sc_key" "$sc_retval" $_sc_usb $_sc_1 $_sc_1x $_sc_2 $_sc_2x $_sc_3 $_sc_3x
					break
				done
			;;
			PUNCT)
				declare reg_token shift_token
				sc_get_token "${_sc_key:0:1}"
				reg_token="$sc_retval"
				sc_set_scan_code "$_id($_case):${_sc_key:0:1}" "$reg_token" $_sc_usb $_sc_1 $_sc_1x $_sc_2 $_sc_2x $_sc_3 $_sc_3x
				sc_get_raw_token "${_sc_key:2:1}"
				shift_token="$sc_retval"
				# Need to set both punctuation characters because the token normalization won't catch it
				sc_set_scan_code "$_id($_case):${_sc_key:2:1}" "$shift_token" $_sc_usb $_sc_1 $_sc_1x $_sc_2 $_sc_2x $_sc_3 $_sc_3x
				declare -g sc_is_shiftpunct_${reg_token}
				printf -v  sc_is_shiftpunct_${reg_token}   "%s" "0"
				declare -g sc_is_shiftpunct_${shift_token}
				printf -v  sc_is_shiftpunct_${shift_token} "%s" "1"
				declare -g sc_is_regpunct_${reg_token}
				printf -v  sc_is_regpunct_${reg_token}     "%s" "1"
				declare -g sc_is_regpunct_${shift_token}
				printf -v  sc_is_regpunct_${shift_token}   "%s" "0"
			;;
			LETTER)
				declare u_token l_token
				sc_get_token "${_sc_key}"
				l_token="$sc_retval"
				sc_set_scan_code "$_id($_case):${_sc_key},," "$l_token" $_sc_usb $_sc_1 $_sc_1x $_sc_2 $_sc_2x $_sc_3 $_sc_3x
				sc_get_raw_token "${_sc_key}"
				u_token="$sc_retval"
				sc_set_scan_code "$_id($_case):${_sc_key}^^" "$u_token" $_sc_usb $_sc_1 $_sc_1x $_sc_2 $_sc_2x $_sc_3 $_sc_3x
				declare -g sc_is_ucase_${u_token}
				printf -v  sc_is_ucase_${u_token} "%s" 1
				declare -g sc_is_ucase_${l_token}
				printf -v  sc_is_ucase_${l_token} "%s" 0
				declare -g sc_is_lcase_${u_token}
				printf -v  sc_is_lcase_${u_token} "%s" 0
				declare -g sc_is_lcase_${l_token}
				printf -v  sc_is_lcase_${l_token} "%s" 1
			;;
			*)
				sc_get_token "$_sc_key"
				sc_set_scan_code "$_id($_case):$_sc_key" "$sc_retval" $_sc_usb $_sc_1 $_sc_1x $_sc_2 $_sc_2x $_sc_3 $_sc_3x
			;;
		esac
	done < <( cat "$SCRIPT_SELF" \
			| sed -n -e '/^##__SCANCODES__/,/^##__EOF__/ { /^#   /p }' \
			| sed -e 's/^#   .* (.*)$/USBALT\0/ ; s/^#   .*\tKP-. \/ .\+$/KPALT\0/ ; s/^#   .*\t. .$/PUNCT\0/ ; s/^#   .*\t[A-Z]$/LETTER\0/ ; s/^#   .*$/OTHER\0/ ; s/#   /\t/' )
}

sc_make_to_break () {
	sc_retval=()

	local mode
	mode="$1"
	shift

	local -a buffer
	case ${mode/x/} in
		s1)
			case $# in
				1) buffer=( $(printf "%02X" $(( 0x$1 | 0x80 )) ) ) ;;
				2) buffer=( $(printf "%s %02X" "$1" $(( 0x$2 | 0x80 )) ) ) ;;
				3) buffer=( $(printf "%s %02X %02X" "$1" $(( 0x$2 | 0x80 )) $(( 0x$3 | 0x80 )) ) ) ;;
			esac
		;;
		s2)
			case $# in
				1) buffer=( F0 $1 ) ;;
				2) buffer=( F0 $1 F0 $2 ) ;;
				*) buffer=( $1 F0 $2 F0 $3 ) ;;
			esac
		;;
		*) buffer=( unimplemented-make_to_break-$mode ) ;;
	esac
	
	sc_retval=( "${buffer[*]}" )
}

sc_convert_token () {
	sc_retval=()

	local mode
	mode="$1"
	shift

	local action
	action="$1"
	shift

	local token
	token="$1"
	shift

	local -a buffer
	local -n sc_ref
	sc_ref=sc_$token
	buffer=( ${sc_ref[$mode]} )

	if [ "$action" = "depress" ]; then
		: # buffer has everything
	elif [ "$action" = "release" ]; then
		sc_make_to_break "$mode" ${buffer[*]}
		# Overwrite the buffer with the break code
		buffer=( $sc_retval )
	elif [ "$action" = "press" ]; then
		sc_make_to_break "$mode" ${buffer[*]}
		# Append the break code to the buffer
		buffer+=( $sc_retval )
	else
		echo "Bad action: $action" >&2
		return 1
	fi

	sc_retval=( "${buffer[*]}" )
}

sc_is_lshift_depressed=0
sc_is_rshift_depressed=0
sc_shift_depress () {
	local mode
	mode="$1"
	shift

	local token
	token="$1"
	shift

	case "${token,,}" in
		lshift)
			if ((sc_is_lshift_depressed)); then return; fi
			sc_is_lshift_depressed=1
		;;
		rshift)
			if ((sc_is_rshift_depressed)); then return; fi
			sc_is_rshift_depressed=1
		;;
		*)
			echo "Bad Shift Modifier: $token" >&2
			return 1
		;;
	esac

	sc_get_token "$token"
	sc_convert_token "$mode" depress "$sc_retval"
}
sc_shift_release () {
	local mode
	mode="$1"
	shift

	local token
	token="$1"
	shift

	case "${token,,}" in
		lshift)
			if (( ! sc_is_lshift_depressed )); then return; fi
			sc_is_lshift_depressed=0
		;;
		rshift)
			if (( ! sc_is_rshift_depressed )); then return; fi
			sc_is_rshift_depressed=0
		;;
		*)
			echo "Bad Shift Modifier: $token" >&2
			return 1
		;;
	esac

	sc_get_token "$token"
	sc_convert_token "$mode" release "$sc_retval"
}

sc_convert_text () {
	sc_retval=()
	local -a buffer
	buffer=()

	local mode
	mode="$1"
	shift

	local _sc_space_press
	sc_get_token space
	sc_convert_token "$mode" press $sc_retval
	_sc_space_press="$sc_retval"

	local auto_shift
	auto_shift=0
	local auto_unshift
	auto_unshift=0

	local text
	for text in "$@"; do
		local is_orig_lshift_depressed
		is_orig_lshift_depressed=$(( sc_is_lshift_depressed ))
		local is_orig_rshift_depressed
		is_orig_rshift_depressed=$(( sc_is_rshift_depressed ))

		local -a ascii
		ascii=( $(printf "%s" "$text" | hexdump -ve '/1 "%02x "') )
		local token
		local ind
		for ((ind=0; ind < ${#ascii[@]}; ind++)); do
			token="${ascii[ind]}"
			# Space is kinda special...
			if [ "$token" = "20" ]; then
				buffer+=( $_sc_space_press )
				continue
			fi

			# All of this is needed to correctly determine which shift key needs pressed or released :-/
			local -n is_lcase_token
			is_lcase_token=sc_is_lcase_${token}
			local -n is_ucase_token
			is_ucase_token=sc_is_ucase_${token}
			local -n is_shiftpunct_token
			is_shiftpunct_token=sc_is_shiftpunct_${token}
			local -n is_regpunct_token
			is_regpunct_token=sc_is_regpunct_${token}

			if [ $(( ${is_ucase_token:-0} | ${is_shiftpunct_token:-0} )) -eq 1 ]; then
				if (( ! ( sc_is_lshift_depressed | sc_is_rshift_depressed ) )); then
					if (( is_orig_rshift_depressed )); then
						sc_shift_depress "$mode" rshift
						buffer+=( $sc_retval )
						auto_unshift=0
					fi
					if (( is_orig_lshift_depressed )); then
						sc_shift_depress "$mode" lshift
						buffer+=( $sc_retval )
						auto_unshift=0
					fi
					if (( ! ( sc_is_lshift_depressed | sc_is_rshift_depressed ) )); then
						sc_shift_depress "$mode" lshift
						buffer+=( $sc_retval )
						auto_shift=1
					fi
				fi
			elif [ $(( ${is_lcase_token:-0} | ${is_regpunct_token:-0} )) -eq 1 ]; then
				if (( sc_is_rshift_depressed )); then
					sc_shift_release "$mode" rshift
					buffer+=( $sc_retval )
					if (( is_orig_rshift_depressed )); then
						auto_unshift=1
					else
						auto_shift=0
					fi
				fi
				if (( sc_is_lshift_depressed )); then
					sc_shift_release "$mode" lshift
					buffer+=( $sc_retval )
					if (( is_orig_lshift_depressed )); then
						auto_unshift=1
					else
						auto_shift=0
					fi
				fi
			fi

			# Finally, press the desired key
			sc_convert_token $mode press $token
			buffer+=( $sc_retval )
		done

		# End of token, reset shift modifiers
		if (( auto_shift )); then
			if (( sc_is_lshift_depressed & ( ! is_orig_lshift_depressed ) )); then
				sc_shift_release "$mode" lshift
				buffer+=( $sc_retval )
			fi
			if (( sc_is_rshift_depressed & ( ! is_orig_rshift_depressed ) )); then
				sc_shift_release "$mode" rshift
				buffer+=( $sc_retval )
			fi
			auto_shift=0
		elif (( auto_unshift )); then
			if (( ( ! sc_is_lshift_depressed ) & is_orig_lshift_depressed )); then
				sc_shift_depress "$mode" lshift
				buffer+=( $sc_retval )
			fi
			if (( ( ! sc_is_rshift_depressed ) & is_orig_rshift_depressed )); then
				sc_shift_depress "$mode" rshift
				buffer+=( $sc_retval )
			fi
			auto_unshift=0
		fi
	done

	sc_retval=( "${buffer[*]}" )
}

# Parses a seq into a series of discrete keyops composed of of an action
# (press, hold, unhold, depress, release, text) and a key-name (or string of
# text in the case of the 'text' action) separated by ':'. (And yes, I am using 'text'
# as a verb.) Some keyops serve as comments and start with '#'. More on that
# below. At the end of the series, keyops are added starting with
# the word 'unhold' and having the key-name of the previous 'hold' keyops in the 
# reverse order it was held.
# Returns an array of '$action $keyname_or_text' keyops.
sc_parse_seq () {
	sc_retval=()

	local seq
	seq="$1"
	shift

	local -a buffer
	buffer=()
	local action
	local keyname
	local -a keyname_chars
	local rep_count
	local rep_keyop
	local open_brace
	local close_brace
	local -a held_keys
	local ind
	for ((ind=0; ind<${#seq}; ind++)); do
		case "${action:-}" in
			"")
				case "${seq:$ind:1}" in
					"{") action=press;   open_brace="${seq:$ind:1}"; close_brace="}"; ;;
					"[") action=press;   open_brace="${seq:$ind:1}"; close_brace="]"; ;;
					"+") action=depress; open_brace=; close_brace=; ;;
					"-") action=release; open_brace=; close_brace=; ;;
					"=") action=hold;    open_brace=; close_brace=; ;;
					":") action=text;    ;;
					*)
						if [ ${#buffer[@]} -gt 0 ]; then
							printf "Raw text can only be used at start of SEQ; error at index %d of %q\n" $ind "$seq" >&2
							return 1
						fi
						buffer+=( "text ${seq:$ind}" )
						break
					;;
				esac
			;;
			depress|release|hold|press)
				if [ -z "${open_brace:-}" ]; then
					case "${seq:$ind:1}" in
						"{") open_brace="${seq:$ind:1}"; close_brace="}"; ;;
						"[") open_brace="${seq:$ind:1}"; close_brace="]"; ;;
						*)
							printf "Expected open-brace at %d of %q\n" $ind "$seq" >&2
							return 1
						;;
					esac
				else
					if [ "${seq:$ind:1}" = "${close_brace}" ]; then
						IFS="" eval 'keyname="${keyname_chars[*]}"'
						keyname_chars=()
						buffer+=( "${action} ${keyname}" )
						if [ "$action" = "hold" ]; then
							held_keys+=( "${keyname}" )
						fi
						keyname=
						if [ "${seq:$(( ind+1 )):1}" = "x" ]; then
							action=repeat-flag
						else
							action=
						fi
						open_brace=
						close_brace=
					else
						keyname_chars+=( "${seq:$ind:1}" )
					fi
				fi
			;;
			repeat-flag)
				action=repeat-parse
				rep_count=0
			;;
			repeat-parse)
				case "${seq:$ind:1}" in
					1|2|3|4|5|6|7|8|9|0) rep_count=$(( (rep_count*10) + ${seq:$ind:1} )) ;;
					*)
						printf "Bad repitition count at index %d of %q" $ind "$seq" >&2
						return 1
					;;
				esac
				case "${seq:$(( ind + 1 )):1}" in
					"["|"{"|"")
						rep_keyop="${buffer[${#buffer[@]} - 1]}"
						while (( rep_count --> 1 )); do
							buffer+=( "${rep_keyop}" )
						done
						action=
					;;
				esac
			;;
			text)
				buffer+=( "${action} ${seq:$ind}" )
				break
			;;
			*)
				printf "ASSERT: action in ''|depress|release|hold|press|text violated; action=%q\n" "$action" >&2
				return 1
			;;
		esac
	done

	if [ -n "${close_brace:-}" ]; then
		IFS="" eval 'keyname="${keyname_chars[*]}"'
		echo "ERROR: Missing '${close_brace}' in '${keyname}'" >&2
		return 1
	fi

	if [ -n "${held_keys+has-held-keys}" ]; then
		for (( ind=${#held_keys[@]}; ind --> 0; )); do
			buffer+=( "unhold ${held_keys[ind]}" )
		done
	fi

	sc_retval=( "${buffer[@]}" )
}

sc_convert_keyops () {
	sc_retval=()
	
	local mode
	mode="$1"
	shift

	local -a buffer
	local keyop
	for keyop in "$@"; do
		local keyop_action item
		keyop_action="${keyop%% *}"
		item="${keyop#* }"
		local action
		case "$keyop_action" in
			hold) action=depress ;;
			unhold) action=release ;;
			*) action="${keyop_action}" ;;
		esac
		case "$action" in
			text)
				sc_convert_text "$mode" "$item"
				buffer+=( ${sc_retval[*]} )
			;;
			press)
				case "${item,,}" in
					lshift|rshift)
						sc_shift_depress "$mode" "$item"
						buffer+=( ${sc_retval[*]} )
						sc_shift_release "$mode" "$item"
						buffer+=( ${sc_retval[*]} )
					;;
					*)
						sc_get_token "$item"
						sc_convert_token "$mode" "$action" "$sc_retval"
						buffer+=( ${sc_retval[*]} )
					;;
				esac
			;;
			depress|release)
				case "${item,,}" in
					lshift|rshift)
						sc_shift_${action} "$mode" "$item"
						buffer+=( ${sc_retval[*]} )
					;;
					*)
						sc_get_token "$item"
						sc_convert_token "$mode" "$action" "$sc_retval"
						buffer+=( ${sc_retval[*]} )
					;;
				esac
			;;
		esac
	done

	sc_retval=( "${buffer[*]}" )
}

SC_HELP_REQUESTED=0
sc_parse_args () {
	sc_retval=()

	local ARG
	while [ -n "${1:-}" ]; do
		ARG="$1"
		shift
		case "$ARG" in
			--help|-h)
				SC_HELP_REQUESTED=1
			;;
			--)
				break
			;;
			-*)
				echo "Bad arg: $ARG" >&2
				return 1
			;;
			*)
				sc_retval+=( "$ARG" )
				break
			;;
		esac
	done
	if [ -n "${1:-}" ]; then
		sc_retval+=( "$@" )
	fi
}

sc_main () {
	sc_parse_args "$@"
	if ((SC_HELP_REQUESTED)); then
		sc_cat_help
		return 0
	fi

	local mode
	mode="${sc_retval[0]:-}"
	case "${mode:-}" in
		1|1x|2|2x|3|3x) mode=s$mode ;;
		usb) mode=$mode ;;
		"")
			echo "Expected: Arg 1 must be a scancode set" >&2
			return 1
		;;
		*)
			echo "Not a valid scancode set: $mode" >&2
			return 1
		;;
	esac
	if [ ${#sc_retval[@]} -lt 2 ]; then
		return 0
	fi
	local -a sequences
	sequences=( "${sc_retval[@]:1}" )
	local -a buffer
	local seq
	for seq in "${sequences[@]}"; do
		sc_parse_seq "$seq"
		sc_convert_keyops "$mode" "${sc_retval[@]}"
		buffer+=( ${sc_retval[@]} )
	done
	
	echo "${buffer[*]}"
}

sc_init () {
	parse_scan_code_table
}

sc_init
if [ -z "${SC_IS_SOURCING:-}" ]; then
	sc_main "$@"
	exit
fi

#   taken directly from http://www.win.tue.nl/~aeb/linux/kbd/scancodes-10.html
#   with a few minor fixups
#   
#   For Set 1, if the make code of a key is c, the break code will be c+0x80.
#   If the make code is e0 c, the break code will be e0 c+0x80. The Pause key 
#   has make code e1 1d 45 e1 9d c5 and does not generate a break code.
#   
#   For Set 2, if the make code of a key is c, the break code will be f0 c. 
#   If the make code is e0 c, the break code will be e0 f0 c. 
#   The Pause key has the 8-byte make code e1 14 77 e1 f0 14 f0 77.
#   
#   For Set 3, by default most keys do not generate a break code - only 
#   CapsLock, LShift, RShift, LCtrl and LAlt do. However, by default all 
#   non-traditional keys do generate a break code - thus, LWin, RWin, Menu do, 
#   and for example on the Microsoft Internet keyboard, so do Back, Forward, 
#   Stop, Mail, Search, Favorites, Web/Home, MyComputer, Calculator, Sleep. 
#   On my BTC keyboard, also the Macro key does.
#   
#	In Scancode Mode 3 it is possible to enable or disable key repeat and the 
#   production of break codes either on a key-by-key basis or for all keys at 
#   once. And just like for Set 2, key release is indicated by a f0 prefix in 
#   those cases where it is indicated. There is nothing special with the 
#   Pause key in scancode mode 3.
#   
#   (USB codes in decimal, scancodes in hex.)
#   
#   #	USB	Set 1	X(Set 1)	Set 2	X(Set 2)	Set 3	X(Set 3)	keycap
##__SCANCODES__
#   1	53	29	39	0e	29	0e	29	` ~
#   2	30	2	41	16	2	16	2	1 !
#   3	31	3	3f	1e	3	1e	3	2 @
#   4	32	4	3d	26	4	26	4	3 #
## I am convinced this is a mistake of the manual conversion
##   6	34	06	3c	2e	06	2e	06	5 % E
#   5	33	5	3b	25	5	25	5	4 $
#   6	34	6	3c	2e	6	2e	6	5 %
#   7	35	7	58	36	7	36	7	6 ^
#   8	36	8	64	3d	8	3d	8	7 &
#   9	37	9	44	3e	9	3e	9	8 *
#   10	38	0a	42	46	0a	46	0a	9 (
#   11	39	0b	40	45	0b	45	0b	0 )
#   12	45	0c	3e	4e	0c	4e	0c	- _
## I am convinced this is a mistake of the manual conversion
##   13	46	0d	0f	55	0d	55	0d	Err:520
#   13	46	0d	0f	55	0d	55	0d	= +
#   15	42	0e	29	66	0e	66	0e	Backspace
#   16	43	0f	59	0d	0f	0d	0f	Tab
#   17	20	10	65	15	10	15	10	Q
#   18	26	11	38	1d	11	1d	11	W
#   19	8	12	2a	24	12	24	12	E
#   20	21	13	70	2d	13	2d	13	R
#   21	23	14	1d	2c	14	2c	14	T
#   22	28	15	10	35	15	35	15	Y
#   23	24	16	2	3c	16	3c	16	U
#   24	12	17	5a	43	17	43	17	I
#   25	18	18	66	44	18	44	18	O
#   26	19	19	71	4d	19	4d	19	P
#   27	47	1a	2c	54	1a	54	1a	[ {
#   28	48	1b	1f	5b	1b	5b	1b	] }
#   29	49	2b	21	5d	2b	5c	75	\ |
#   30	57	3a	32	58	3a	14	1d	CapsLock
#   31	4	1e	3	1c	1e	1c	1e	A
#   32	22	1f	5b	1b	1f	1b	1f	S
#   33	7	20	67	23	20	23	20	D
#   34	9	21	2e	2b	21	2b	21	F
#   35	10	22	2d	34	22	34	22	G
#   36	11	23	20	33	23	33	23	H
#   37	13	24	12	3b	24	3b	24	J
#   38	14	25	5	42	25	42	25	K
#   39	15	26	4	4b	26	4b	26	L
#   40	51	27	5c	4c	27	4c	27	; :
#   41	52	28	68	52	28	52	28	' "
#   42	50	0	ff	0	ff	0	ff	non-US-1
#   43	40	1c	1e	5a	1c	5a	1c	Enter
#   44	225	2a	2f	12	2a	12	2a	LShift
#   46	29	2c	14	1a	2c	1a	2c	Z
#   47	27	2d	13	22	2d	22	2d	X
#   48	6	2e	6	21	2e	21	2e	C
#   49	25	2f	5d	2a	2f	2a	2f	V
#   50	5	30	69	32	30	32	30	B
#   51	17	31	31	31	31	31	31	N
#   52	16	32	30	3a	32	3a	32	M
#   53	54	33	23	41	33	41	33	, <
#   54	55	34	22	49	34	49	34	. >
#   55	56	35	15	4a	35	4a	35	/ ?
#   57	229	36	7	59	36	59	36	RShift
#   58	224	1d	11	14	1d	11	38	LCtrl
#   60	226	38	6a	11	38	19	71	LAlt
#   61	44	39	72	29	39	29	39	space
#   62	230	e0-38	e0-6a	e0-11	e0-38	39	72	RAlt
#   64	228	e0-1d	e0-11	e0-14	e0-1d	58	3a	RCtrl
#   75	73	e0-52	e0-28	e0-70	e0-52	67	7b	Insert
#   76	76	e0-53	e0-74	e0-71	e0-53	64	79	Delete
#   80	74	e0-47	e0-60	e0-6c	e0-47	6e	7f	Home
#   81	77	e0-4f	e0-61	e0-69	e0-4f	65	7a	End
#   85	75	e0-49	e0-34	e0-7d	e0-49	6f	6f	PgUp
#   86	78	e0-51	e0-73	e0-7a	e0-51	6d	7e	PgDn
#   79	80	e0-4b	e0-26	e0-6b	e0-4b	61	56	Left
#   83	82	e0-48	e0-6c	e0-75	e0-48	63	78	Up
#   84	81	e0-50	e0-6d	e0-72	e0-50	60	55	Down
#   89	79	e0-4d	e0-19	e0-74	e0-4d	6a	7d	Right
#   90	83	45	0b	77	45	76	1	NumLock
#   91	95	47	60	6c	47	6c	47	KP-7 / Home
#   92	92	4b	26	6b	4b	6b	4b	KP-4 / Left
#   93	89	4f	61	69	4f	69	4f	KP-1 / End
#   95	84	e0-35	e0-15	e0-4a	e0-35	77	45	KP-/
#   96	96	48	6c	75	48	75	48	KP-8 / Up
#   97	93	4c	27	73	4c	73	4c	KP-5
#   98	90	50	6d	72	50	72	50	KP-2 / Down
#   99	98	52	28	70	52	70	52	KP-0 / Ins
#   100	85	37	5e	7c	37	7e	46	KP-*
#   101	97	49	34	7d	49	7d	49	KP-9 / PgUp
#   102	94	4d	19	74	4d	74	4d	KP-6 / Right
#   103	91	51	73	7a	51	7a	51	KP-3 / PgDn
#   104	99	53	74	71	53	71	53	KP-. / Del
#   105	86	4a	35	7b	4a	84	54	KP--
#   106	87	4e	0c	79	4e	7c	37	KP-+
#   108	88	e0-1c	e0-1e	e0-5a	e0-1c	79	4e	KP-Enter
#   110	41	1	43	76	1	8	64	Esc
#   112	58	3b	24	5	3b	7	58	F1
#   113	59	3c	16	6	3c	0f	59	F2
#   114	60	3d	8	4	3d	17	5a	F3
#   115	61	3e	9	0c	3e	1f	5b	F4
#   116	62	3f	5f	3	3f	27	5c	F5
#   117	63	40	6b	0b	40	2f	5d	F6
#   118	64	41	33	83	41	37	5e	F7
#   119	65	42	25	0a	42	3f	5f	F8
#   120	66	43	17	1	43	47	60	F9
#   121	67	44	18	9	44	4f	61	F10
#   122	68	57	6e	78	57	56	62	F11
#   123	69	58	3a	7	58	5e	63	F12
#   124	70	e0-37	e0-5e	e0-7c	e0-37	57	6e	PrtScr
#   0	154	54	1a	84	54	57	6e	Alt+SysRq
#   125	71	46	0a	7e	46	5f	76	ScrollLock
#   126	72	e1-1d-45	e1-11-0b	e1-14-77	e1-1d-45	62	77	Pause
#   0	0	e0-46	e0-0a	e0-7e	e0-46	62	77	Ctrl+Break
#   0	227	e0-5b	e0-1b	e0-1f	e0-5b	8b	8b	LWin (USB: LGUI)
#   0	231	e0-5c	e0-75	e0-27	e0-5c	8c	8c	RWin (USB: RGUI)
#   0	0	e0-5d	e0-2b	e0-2f	e0-5d	8d	8d	Menu
#   0	0	e0-5f	e0-76	e0-3f	e0-5f	7f	54	Sleep
#   0	0	e0-5e	e0-63	e0-37	e0-5e	0	ff	Power
#   0	0	e0-63	e0-78	e0-5e	e0-63	0	ff	Wake
##__EOF__
