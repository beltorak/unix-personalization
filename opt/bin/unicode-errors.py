#!/usr/bin/env python3
import sys
for file in sys.argv[1:]:
	lineno=0
	with open(file, 'r', encoding='utf8', errors='surrogateescape') as fh:
		for line in fh.read().splitlines():
			lineno += 1
			try:
				line.encode('utf8')
			except UnicodeEncodeError as exc:
				if exc.args[4] == 'surrogates not allowed':
					print("{file}:{lineno}:{line!r}".format(**locals()))
				else:
					print("Unexpected Error: {file}:{lineno}\n    {exc!r}".format(locals()))
