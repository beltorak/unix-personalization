alias grep='grep --color'
alias clear="echo -ne '\033c'"
alias cls=clear
alias ls='ls --color=auto -F'
alias cmd-here='cmd /c start "" cmd /k "cd /d $(cygpath --windows $PWD)"'

function title() {
	case "$TERM" in
		xterm|*rxvt*)
#			local STATIC_PROMPT='\n\[\e[32m\]\u\e[37m\]@[\e[32m\]\h\e[37m\]] \[\e[33m\]\w\[\e[0m\]\n\$ '
			local NL='\n\['
			local GREEN='\e[32m\]'
			local YELLOW='\e[33m\]'
			local CYAN='\e[36m\]'
			local OFF='\e[0m\]'
			local STATIC_PROMPT=${NL}${GREEN}'\u'${CYAN}'@'${GREEN}'\h'${OFF}' \['${YELLOW}'\w\['${OFF}${NL}'\$ '
			if [ -z "$*" ]; then
				export PS1='\[\e]0;\w\a\]'$STATIC_PROMPT
			else
				echo -ne "\033]2;$*\007"
				export PS1=$STATIC_PROMPT
			fi
			;;
	esac
}
title

bind 'set match-hidden-files off'
bind 'set completion-ignore-case on'

export PATH=/opt/bin:$PATH

